public class Sample {
    public void Example1A(object input)
    {
        object result;
        if (LoadFrom(input) != null)
        {
            result = LoadFrom(input);
        }
        else
        {
            result = AlternateLoadFrom(input);
        }
    }

    public void Example1B(object input) {
        var result = LoadFrom(input) != null ? LoadFrom(input) : AlternateLoadFrom(input);
    }

    public void Example2(object input)
    {
        object result = LoadFrom(input);
        if (result == null)
        {
            result = AlternateLoadFrom(input);
        }
    }

    private object LoadFrom(object o) {return null;}
    private object AlternateLoadFrom(object o) {return null;}
}