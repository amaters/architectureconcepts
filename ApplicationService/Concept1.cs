using AntonMaters.ArchitectureConcepts.ApplicationService.Models;
using AntonMaters.ArchitectureConcepts.ApplicationService.Interfaces;

namespace AntonMaters.ArchitectureConcepts.ApplicationService
{
    public class Concept1
    {
        private readonly ILogger logger;
        private readonly ISomeClient client;

        private SubjectObject subject;

        public Concept1(ILogger logger, ISomeClient client, SubjectObject subject)
        {
            this.logger = logger;
            this.client = client;
            this.subject = subject;
         }

         public ResultObject Execute()
         {
             var result = ResultObject.Create();

             //Do stuff with subject and set things in result;

             return result;
         }
    }
}