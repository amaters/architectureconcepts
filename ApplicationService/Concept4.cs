using AntonMaters.ArchitectureConcepts.ApplicationService.Models;
using AntonMaters.ArchitectureConcepts.ApplicationService.Interfaces;

namespace AntonMaters.ArchitectureConcepts.ApplicationService
{
    public class Concept4
    {
        private readonly ILogger logger;
        private readonly ISomeClient client;

        private SubjectObject subject;
        public ResultObject Result;

        public Concept4(ILogger logger, ISomeClient client)
        {
            this.logger = logger;
            this.client = client;
        }
        public bool MichielIsAlwaysRight = true;

        public Concept4 SetSubject(SubjectObject input)
        {
            //do some validation
            subject = input;
            return this;
        }
        public void Execute()
        {
            var result = ResultObject.Create();

            //Do stuff with subject and set things in result;
        }
    }
}