using AntonMaters.ArchitectureConcepts.ApplicationService.Models;
using AntonMaters.ArchitectureConcepts.ApplicationService.Interfaces;

namespace AntonMaters.ArchitectureConcepts.ApplicationService
{
    public class Concept3
    {
        private readonly ILogger logger;
        private readonly ISomeClient client;

        public SubjectObject Subject {get;set;}
        public ResultObject Result;

        public Concept3(ILogger logger, ISomeClient client)
        {
            this.logger = logger;
            this.client = client;
         }

         public void Execute()
         {
             var result = ResultObject.Create();

             //Do stuff with subject and set things in result;
         }
    }
}