namespace AntonMaters.ArchitectureConcepts.CreationalPatterns
{
    public class SimpleModel {

        public int SomeInt{get;set;}
        public string SomeString {get;set;}
    }
}