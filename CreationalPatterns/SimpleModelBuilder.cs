
namespace AntonMaters.ArchitectureConcepts.CreationalPatterns
{
    public class SimpleModelBuilder {
        private int someInt;
        private string someString;

        public SimpleModelBuilder SetIntegers(int integer1) {
            someInt = integer1;
            return this;
        }
        public SimpleModelBuilder SetText(string text1) {
            someString = text1;
            return this;
        }

        public SimpleModel Build() {
            return new SimpleModel{
                    SomeInt = someInt,
                    SomeString = someString
            }; 
        }
    }
}