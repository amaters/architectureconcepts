
namespace AntonMaters.ArchitectureConcepts.CreationalPatterns
{
    public class Main
    {
        public void Model()
        {
            SimpleModel model = new SimpleModel{
                    SomeInt = 1,
                    SomeString = "def"
            };
         }

         public void Create() {
             ModelWithPrivateConstructor model = ModelWithPrivateConstructor.Create(1, "def");
         }

         public void Factory() {
            SimpleModel model = new SimpleModelFactory().Create(1, "def");
         }

         public void Builder() {
            SimpleModel model = new SimpleModelBuilder()
                .SetIntegers(1)
                .SetText("def")
                .Build();
            
         }
    }
}