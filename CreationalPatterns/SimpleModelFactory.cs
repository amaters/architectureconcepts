namespace AntonMaters.ArchitectureConcepts.CreationalPatterns
{
    public class SimpleModelFactory
    {
        private const int DEFAULT_INT = 3;
        private const string DEFAULT_STRING = "xyz";

        public SimpleModel Create() {
            return this.Create(DEFAULT_INT, DEFAULT_STRING);
        }
        
        public SimpleModel Create(int someInt, string someString)
        {
            return new SimpleModel
            {
                SomeInt = someInt,
                SomeString = someString
            };
        }
    }
}

