namespace AntonMaters.ArchitectureConcepts.CreationalPatterns
{
    public class ModelWithPrivateConstructor
    {
        private ModelWithPrivateConstructor() { }

        public int SomeInt { get; set; }
        public string SomeString { get; set; }
        public static ModelWithPrivateConstructor Create(int someInt, string someString)
        {
            return new ModelWithPrivateConstructor
            {
                SomeInt = someInt,
                SomeString = someString
            };

        }
        public static ModelWithPrivateConstructor Create()
        {
            return Create(10, "abc");
        }
    }
}

